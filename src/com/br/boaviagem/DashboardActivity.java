package com.br.boaviagem;

import android.os.Bundle;
import android.app.Activity;
import android.view.*;

public class DashboardActivity extends Activity {

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);
		
		
	}
	
	public void selecionarOpcao(View view){
		switch (view.getId()) {
		case  R.id.nova_viagem:
			setContentView(R.layout.cadastro_viagem);
			break;
		case R.id.novo_gasto:
			setContentView(R.layout.gasto);
		}
	}
}
